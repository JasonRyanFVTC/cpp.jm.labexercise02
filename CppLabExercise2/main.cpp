#include <iostream>
#include <conio.h>
#include <string>


/* Enum representing the available playing card suits */
enum CardSuit {
	CS_HEARTS,
	CS_DIAMONDS,
	CS_SPADES,
	CS_CLUBS
};

/* Enum representing the available playing card ranks */
enum CardRank {
	CR_TWO = 2,
	CR_THREE = 3,
	CR_FOUR = 4,
	CR_FIVE = 5,
	CR_SIX = 6,
	CR_SEVEN = 7,
	CR_EIGHT = 8,
	CR_NINE = 9,
	CR_TEN = 10,
	CR_JACK = 11,
	CR_QUEEN = 12,
	CR_KING = 13,
	CR_ACE = 14
};

/* Represents a playing Card in memory */
struct PlayingCard {
	CardSuit Suit;
	CardRank Rank;
};

void PrintCard(PlayingCard card)
{
	std::string rankName;
	switch (card.Rank)
	{
		case CR_TWO: rankName = "Two"; break; 
		case CR_THREE: rankName = "Three"; break;
		case CR_FOUR: rankName = "Four"; break;
		case CR_FIVE: rankName = "Five"; break;
		case CR_SIX: rankName = "Six"; break;
		case CR_SEVEN: rankName = "Seven"; break;
		case CR_EIGHT: rankName = "Eight"; break;
		case CR_NINE: rankName = "Nine"; break;
		case CR_TEN: rankName = "Ten"; break;
		case CR_JACK: rankName = "Jack"; break;
		case CR_QUEEN: rankName = "Queen"; break;
		case CR_KING: rankName = "King"; break;
		case CR_ACE: rankName = "Ace"; break;
		default: "None";
	}
	std::string suitName;
	switch (card.Suit)
	{
		case CS_HEARTS: suitName = "Hearts"; break;
		case CS_DIAMONDS: suitName = "Diamonds"; break;
		case CS_SPADES: suitName = "Spades"; break;
		case CS_CLUBS: suitName = "Clubs"; break;
		default: "None";
	}
	std::cout << "The " << rankName << " of " << suitName;
}
PlayingCard HighCard(PlayingCard card1, PlayingCard card2)
{
	if (card1.Rank > card2.Rank)
		// Card 1 rank is higher
		return card1;
	else if (card1.Rank < card2.Rank)
		// Card 2 rank is higher
		return card2;
	else
	{
		// At this point, Card 1 and Card 2 have the same rank and their suit values are now used to determine their ranks
		// Suit values are determined based on their enum value
		if (card1.Suit > card2.Rank)
			return card1;
		else if (card1.Suit < card2.Rank)
			return card2;
		else
			// Card 1 and Card 2 are technically just the same card
			return card1;
	}
}

/* Main entry point for the Lab Exercise 2 Application */
int main()
{
	// Initialize Card 1
	PlayingCard card1;
	card1.Rank = CR_KING;
	card1.Suit = CS_HEARTS;

	// Initialize Card 2
	PlayingCard card2;
	card2.Rank = CR_FOUR;
	card2.Suit = CS_CLUBS;

	// Print Card 1
	std::cout << "Card 1: ";
	PrintCard(card1);
	std::cout << std::endl;
	// Print Card 2
	std::cout << "Card 2: ";
	PrintCard(card2);
	std::cout << std::endl << std::endl;
	// Print card that has higher rank
	PlayingCard highCard = HighCard(card1, card2);

	std::cout << "Higher Rank: ";
	PrintCard(highCard);
	
	_getch();
	return 0;
}
